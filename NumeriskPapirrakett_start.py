# -*- coding: utf-8 -*-
"""
Dette er en enkel kodesnutt for numerisk beregning av rakettbaner gitt konstant
masse og kjent utgangshastighet.

OBS: Koden er ikke ferdig!!!

Andøya Space Education

Opprettet 11:06:56 fredag 9. september 2022
Sist endret [dd.mm.åååå]: 16.09.2024
@forfatter: bjarne.ådnanes.bergtun
"""

# Nyttige biblioteker
import numpy as np # for matematiske beregninger
import matplotlib.pyplot as plt # for plotting



# =============================== Konstanter ================================ #

# Simuleringskonstanter
dt = 0.001 # [s]            tidssteg
t_f = 10 # [s]              sluttid (maksimal lengde på numeriske beregninger)


# Fysiske konstanter
g = 9.81 # [m/s^2]              tyngeakselerasjonen ved havnivå
rho = 1.25 # [kg/m^3]           lufttettheten


# Rakettkonstanter
m = 1.000 # [kg]                rakettens masse
L = 1.000 # [m]                 rakettens effektive indre lengde
C_D = 1 # [-]                   rakettens luftmotstandskoeffesient
A_f = np.pi*1.000**2 # [m^2]    rakettens tverrsnittareal (sett forfra)
A = np.pi*0.010**2 # [m^2]      arealet trykkluften virker mot


# Rampekonstanter
theta = 45 # [deg]                  utskytningsvinkel; målt fra horisontalen
P_0 = 6*10**5 # [Pa]                overtrykk i trykktanken
# V_0 = 2*0.001 # [m^3]               trykktankens volum
# L_rampe = 0.35 # [m]               utskytningsrørets indre lengde
# A_rampe = np.pi*0.01**2 # [m^2]     utskytningsrørets indre tverrsnittareal


# Utskrift av sentrale variabler.
print() # Hopper over en linje. Dette kan også gjøres vha. "\n".
print("masse:\t\t", m*1000, "g")
print("lengde:\t\t", L*100, "cm")
print("overtrykk:\t", P_0/(10**5), "bar")
print("elevasjon:\t", theta, "deg")
print('---------------------')


# Innledende beregninger
v_0 = 0
theta = np.radians(theta) # Kan også gjøres "manuelt": theta*np.pi/180



# ========================== Numeriske beregninger ========================== #

# Først lager vi lister med punktene vi skal bruke i beregningene våre.
# Bortsett fra tids-listen initialiseres alle til (0, 0, 0, ...).
t = np.arange(t_f, step = dt)   # tid; starter på 0
x = np.zeros(np.shape(t))       # bakkeavstand
y = np.zeros(np.shape(t))       # høyde
v_x = np.zeros(np.shape(t))     # horisontal hastighet
v_y = np.zeros(np.shape(t))     # vertikal hastighet
a_x = np.zeros(np.shape(t))     # horisontal akselerasjon
a_y = np.zeros(np.shape(t))     # vertikal akselerasjon



# Initialbetingelser: Posisjon er OK, men vi må sette inn starthastigheten.
v_x[0] = v_0 * np.cos(theta)
v_y[0] = v_0 * np.sin(theta)
# Startakselerasjonen må også regnes ut, men dette kan vi gjøre i while-løkken
# nedenfor.



"""
Om while-løkker:

    En while-løkke er en løkke som evaluerer et uttrykk for å avgjøre om løkken
    skal kjøres (igjen). Dersom uttrykket evalueres til "Sant", kjører løkken
    én gang til -- hvis ikke, brytes løkken og programmet hopper videre til
    første linje utenfor løkken.

    Vi kunne brukt en enkel for-løkke for å fylle inn elemenetene i listene vi
    har laget, men for å spare tid ønsker vi å bryte løkken idet raketten
    krasjer. Dette kan vi oppnå ved å bruke en while-løkke.
    
"""


# Vi starter med å opprette en hjelpevariabel som holder styr på hvor vi er i
# listene (dette kalles for en "indeks")
n = 0 # Python-lister starter på 0
n_max = np.size(t)-1 # maksimal indeks; hvis n > n_max, er vi utenfor listen

# While-løkken vår kan nå skrives slik:
while n < n_max and y[n] >= 0: # Raketten krasjer når y<0; listene slutter når n+1 = n_max
    # Nåværend fart
    v = np.sqrt(v_x[n]**2 + v_y[n]**2) # Potenser angis med ** ("a**2" i stedet for "a^2")
    
    # Nåværende akselersjon
    a_x[n] = 0
    a_y[n] = 0

    # Neste posisjon
    x[n+1] = x[n] + 0
    y[n+1] = y[n] + 0
    
    # Neste hastighet
    v_x[n+1] = v_x[n] + 0
    v_y[n+1] = v_y[n] + 0
    
    # Oppdaterer dummy-variabelen n, slik at vi kan beregne neste steg:
    n += 1


# Løkken ovenfor slutter når raketten krasjer, så dataene vi interesserer oss
# for er fra indeks 0 til n-1. Dermed kan vi korte ned listene våre ved å
# begrense dem til disse indeksene. I Python-språk kan dette skrives som ":n",
# hvilket betyr "alle indekser opp til n".
t = t[:n]
x = x[:n]
y = y[:n]
v_x = v_x[:n]
v_y = v_y[:n]
a_x = a_x[:n]
a_y = a_y[:n]



# ================== Plotting og utskrift av nyttig info ==================== #

# Utskrift av hastigheten idet raketten forlater rampen
print("v_i =", round(v_0,2), "m/s")

# Vi skriver også ut nedslagsstedet (siste posisjon langs x). Python-lister kan
# indekseres fra slutten ved bruk av negative tall, så det siste elementet i x
# er rett og slett x[-1]:
print("x_f =", round(x[-1],2), "m")


# Før vi plotter kan vi lukke alle åpne figurer slik at vi slipper å gjøre det
# manuelt.
plt.close('all')


# Rakettbane
plt.figure('Rakettbane')
plt.plot(x, y)
plt.xlabel("Lengde [m]")
plt.ylabel("Høyde [m]")
plt.grid(linestyle='--')
#plt.gca().set_aspect('equal') # Gjør forholdet mellom x og y lik 1
plt.show()


# Høyde versus tid
plt.figure('Høyde v. tid')
plt.plot(t, y)
plt.xlabel("$t$ [s]")
plt.ylabel("Høyde [m]")
plt.grid(linestyle='--')
plt.show()


# Hastighet versus tid
plt.figure('Hastighet v. tid')
plt.plot(t, v_x, label='$v_x$')
plt.plot(t, v_y, label='$v_y$')
plt.plot(t, np.sqrt(v_x**2 + v_y**2), label=r'$|\vec{v}|$')
plt.xlabel("$t$ [s]")
plt.ylabel("Fart [m/s]")
plt.grid(linestyle='--')
plt.legend()
plt.show()


# Akselerasjon versus tid
plt.figure('Akselerasjon v. tid')
plt.plot(t, a_x, label='$a_x$')
plt.plot(t, a_y, label='$a_y$')
plt.plot(t, -np.sqrt(a_x**2 + a_y**2), label=r'$-|\vec{a}|$')
plt.xlabel("$t$ [s]")
plt.ylabel("Akselerasjon [m/s$^2$]")
plt.grid(linestyle='--')
plt.legend()
plt.show()